package com.itheima.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class EurekaQcApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaQcApplication.class, args);
	}

}
