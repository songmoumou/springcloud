package com.itheima.eureka.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QcController {
    //搭负载均衡的时候,两个yml文件,后面的test01必须跟active profile一样 ,不然就不是多个服务器
    @Value("${myName}")
    String name;

    @GetMapping("/qc")
   public String qc(){
       return  name+"切好了土豆丝";
   }

    @GetMapping("/xg")
    public String xg(){
        return "切好了西瓜";
    }


}
