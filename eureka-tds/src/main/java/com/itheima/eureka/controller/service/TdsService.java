package com.itheima.eureka.controller.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("eureka-qc")//这个名字跟微服务bootstrap中定义的名字必须一样,这样才能找到微服务,并调用
public interface TdsService {

        @GetMapping("/qc") //这个映射的地址,跟要调用的controller方法的映射地址一样
        String qc();
        @GetMapping("xg")
        String xg();

        //要使用feign 也就是要用此微服务调用其他的微服务,就在此服务上加入openfeign依赖
    //并且需要一个接口,也就是此接口,去找到要调用的微服务,同时还要再起使类中加入@EnableFeignClients注解
}
