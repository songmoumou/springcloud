package com.itheima.eureka.controller;


import com.itheima.eureka.controller.service.TdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TdsController {

    @Autowired
    TdsService tdsService;
    @GetMapping("/tds")
    public String tds(){
        String qc = tdsService.qc();

       return qc+",一盘土豆丝";
    }
    @GetMapping("/xg")
    public String xg(){
        String xg = tdsService.xg();

        return xg+",一盘土豆丝";
    }

    @GetMapping("/all")
    public String all(){
        String xg = tdsService.xg();
        String qc = tdsService.qc();
        return xg+",一盘土豆丝"+qc;
    }


}
